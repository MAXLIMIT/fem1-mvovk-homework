const tableCreate = document.createElement('table')

for (let trCreate = 0; trCreate <= 30; trCreate++) {
    let tr = document.createElement('tr')

    for (let tdCreate = 0; tdCreate <= 30; tdCreate++) {
        let td = document.createElement('td')

        tr.appendChild(td)
        tableCreate.appendChild(tr)
        document.body.appendChild(tableCreate)
        tableCreate.style.border = '1px solid black'
        tableCreate.style.height = 600 + 'px'
        tableCreate.style.width = 600 + 'px'
        td.style.border = '1px solid black'
        td.className = 'no-selected'
    }
}

const body = document.querySelector('body')
const tbl = document.querySelector('table')
const cell = document.querySelectorAll('td')

tbl.addEventListener('click', (event) => {
    cell.forEach(el => {
        if (event.target === el && el.classList.contains('no-selected')) {
            el.classList.add('selected')
            el.classList.remove('no-selected')
        } else if (event.target === el && el.classList.contains('selected')) {
            el.classList.add('no-selected')
            el.classList.remove('selected')
        }
        event.stopPropagation()
    })
});

body.addEventListener('click', (event) => {
    if (tbl.classList.contains('inverted') === false) {
        tbl.className = 'inverted'
    } else {
        tbl.classList.remove('inverted')
    }
})