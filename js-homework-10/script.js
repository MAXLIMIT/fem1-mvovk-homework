const firstInput = document.querySelector('.first-input');
const secondInput = document.querySelector('.second-input');
const submitBtn = document.querySelector('.btn');
const inputWrapper = document.querySelector('.second-input-wrapper');
const icon = document.querySelectorAll('.icon');
const errorMessage = document.createElement('p');

errorMessage.innerText = 'Нужно ввести одинаковые значения';
errorMessage.style.color = 'red';
errorMessage.style.display = 'none';
inputWrapper.appendChild(errorMessage);

icon.forEach((element) => {
    element.addEventListener('click', () => {
        if (element.classList.contains('fa-eye')) {
            element.classList.remove('fa-eye');
            element.classList.add('fa-eye-slash');
            element.previousElementSibling.setAttribute('type', 'password')
        } else {
            element.classList.remove('fa-eye-slash');
            element.classList.add('fa-eye');
            element.previousElementSibling.setAttribute('type', 'text')
        }
    })
});

submitBtn.addEventListener('click', () => {
   if (firstInput.value === secondInput.value) {
       alert('You are welcome');
       errorMessage.style.display = 'none'
   } else {
       errorMessage.style.display = 'block'
   }
});
