const input = document.getElementById('user-input');
const price = document.createElement('span');
const close = document.createElement('span');
const error = document.createElement('p');

input.addEventListener('focus', ()=> {
    input.style.outlineColor = 'green';
});

input.parentNode.insertBefore(price, input);
document.body.appendChild(error);
price.style.display = 'none';
error.style.display = 'none';

input.addEventListener('blur', ()=> {
    price.style.display = 'block';
    price.innerText = `Текущая цена: ${input.value}`;
    error.innerText = 'Please enter correct price';
    price.appendChild(close);
    close.innerText = ' X';
    close.addEventListener('click', () => {
        price.style.display = 'none';
        input.value = '';
    });
    if(input.value < 0) {
        input.style.color = 'black';
        price.style.display = 'none';
        error.style.display = 'block';
        input.style.border = '1px solid red';
    } else {
        input.style.color = 'green';
        error.style.display = 'none';
        input.style.border = '1px solid grey';
    }
});
