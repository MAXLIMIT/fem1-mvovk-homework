let images = document.querySelectorAll('.image-to-show');
let stop = document.getElementById('stop');
let play = document.getElementById('play');

images.forEach((items) => {
   items.style.display = 'none'
});

let currentSlide = 0;
images[currentSlide].style.display = 'block';

const clearCarouselInterval = () => {
    clearInterval(carouselInterval);
    carouselInterval = setInterval(nextCarousel, 1000);
};

const nextCarousel = () => {
    images[currentSlide].style.display = 'none';
    currentSlide = (currentSlide + 1) % images.length;
    images[currentSlide].style.display = 'block';
};

let carouselInterval = setInterval(nextCarousel, 1000);

stop.addEventListener('click', ()=>{
    clearInterval(carouselInterval)
});

play.addEventListener('click', clearCarouselInterval);
