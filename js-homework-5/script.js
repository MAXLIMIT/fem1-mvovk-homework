function creatNewUser(userFirsName, userLastName, userBirthday){
    let newUser = {
        firstName: userFirsName,
        lastName: userLastName,
        birthday: userBirthday
    };

    Object.defineProperty(newUser, 'userAge', {
        get: function getAge() {
            let dateArrayReverse = userBirthday.split('.').reverse().join('.');
            let dateOfBirthday = new Date(dateArrayReverse);
            let ageCalc = new Date() - dateOfBirthday;
            let userAge = Math.round( ageCalc / (24 * 3600 * 365.25 * 1000));
            return (userAge)
        }
    });

    Object.defineProperty(newUser, 'userLogin', {
        get: function getLogin() {
            return (userFirsName.charAt(0) + userLastName).toLowerCase()
        }
    });

    Object.defineProperty(newUser, 'userPassword', {
        get: function getPassword() {
            return (userFirsName.charAt(0).toUpperCase() + userLastName.toLowerCase() + (newUser.birthday.slice(-4)))
        }
    });

    return newUser
}

console.log(creatNewUser(prompt('Enter your first name', ''),
    prompt('Enter your last name', ''),
    prompt('Enter your birthday (dd.mm.yyyy)', '')));