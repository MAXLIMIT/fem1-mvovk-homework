$('a[href^="#"]').click(function () {
    targetLink = $(this).attr("href");
    $('body, html').animate({
        scrollTop: $(targetLink).offset().top
    }, 600);
});

$(window).scroll(function () {
    let windowHeight = $(window).height()
    let scrollHeight = $(window).scrollTop()

    if (scrollHeight > windowHeight) {
        $('.scroll-up').fadeIn()
    } else {
        $('.scroll-up').fadeOut()
    }
});

$('.show-hide-btn').on('click', function slideToggle() {
    if($(this).text() === 'Show') {
        $(this).text('Hide')
        $('.hot-news-items').slideDown()
                            .css('display', 'grid')
    } else {
        $(this).text('Show')
        $('.hot-news-items').slideUp()
    }
})



