//BUTTON LISTENERS

let createBtn = document.getElementById('openModal')
let modal = document.getElementById('modal')
let modalClose = document.getElementById('modalClose')
let stub = document.getElementById('stub')

createBtn.onclick = () => modal.style.display = "flex"
document.body.onclick = (e) => {
	if(e.target === modalClose || e.target === modal)
		modal.style.display = "none"
}


//LOCAL STORAGE SETTER

let lcards = []

function storeObj(obj) {
	lcards.push(obj)
	localStorage.cards = JSON.stringify(lcards)
}

function deleteObj(obj) {
	let index = lcards.indexOf(obj)
	lcards.splice(index, 1)
	localStorage.cards = JSON.stringify(lcards)
}


//CARDS CLASSES

let destinaton = document.getElementById('cardsBlock')     //Container for cards

class Visit{
	constructor(obj){
		let d = new Date()
		this.doctor = obj.doctor
		this.date = obj.date ? obj.date : d.toDateString()
		this.name = obj.name
		this.purpose = obj.purpose
		this.comment = obj.comment
		this._card = document.createElement('div')
		this._card.className = "visit-card"
		this.hiddenInfo = {
			"Date": this.date, 
			"Purpose": this.purpose
		}
    }
	
	buildCard(hidden){
		let closeBtn = document.createElement('button')
		closeBtn.innerText = "X"
		closeBtn.className = "close-btn"
		closeBtn.onclick = () => this.removeCard()
		this._card.appendChild(closeBtn)
		
		let cardName = document.createElement('p')
		cardName.innerText = this.name
		cardName.className = "card-name"
		this._card.appendChild(cardName)

		let cardDoctor = document.createElement('p')
		cardDoctor.innerText = this.doctor
		cardDoctor.className = "card-doctor"
		this._card.appendChild(cardDoctor)

		let hiddenBlock = document.createElement('div')
		hiddenBlock.className = "hidden-block hidden"
		for (let el in hidden){
			let item = document.createElement('p')
			item.innerText = `${el}: ${hidden[el]}`
			hiddenBlock.appendChild(item)
		}
		let comments = document.createElement('p')
		comments.innerText = this.comment
		hiddenBlock.appendChild(comments)
		this._card.appendChild(hiddenBlock)

		let showMore = document.createElement('p')
		showMore.innerText = "Show more"
		showMore.className = "show-more"
		showMore.onclick = () => {
			hiddenBlock.classList.toggle("hidden")
			showMore.innerText = hiddenBlock.classList.contains("hidden") ? "Show more" : "Hide"
		}
		this._card.appendChild(showMore)

		destinaton.appendChild(this._card)
	}

	removeCard(){
		destinaton.removeChild(this._card)
		if(destinaton.children.length === 1)
			stub.style.display = ""
		deleteObj(this)
	}

}

class Cardio extends Visit{
	constructor(obj){
		super(obj)
		this.pressure = obj.pressure
		this.weight = obj.weight
		this.illness = obj.illness
		this.age = obj.age
		Object.assign(this.hiddenInfo,
			{"Pressure": this.pressure, 
			"Weight": this.weight, 
			"Illness": this.illness, 
			"Age": this.age})
		super.buildCard(this.hiddenInfo)

		storeObj(this)
	}
}

class Dentist extends Visit{
	constructor(obj){
		super(obj)
		this.lastVisit = obj.lastVisit
		this.hiddenInfo["Last visit"] = this.lastVisit
		super.buildCard(this.hiddenInfo)

		storeObj(this)
	}
}

class Therapist extends Visit{
	constructor(obj){
		super(obj)
		this.age = obj.age
		this.hiddenInfo["Age"] = this.age
		super.buildCard(this.hiddenInfo)
		
		storeObj(this)
	}
}

function newCard(obj){
		switch(obj.doctor){
			case "Therapist": 
				new Therapist(obj)
				break
			case "Dentist":
				new Dentist(obj)
				break
			case "Cardiologist":
				new Cardio(obj)
				break
		}
		stub.style.display = "none"
}

//CREATING CARDS

//Restore cards from local storage
if(localStorage.cards){
	let cardsArr = JSON.parse(localStorage.cards)
	cardsArr.forEach(el => {
		newCard(el)
	})
}

let form = document.getElementById('form')
let createCard = document.getElementById('createCard')

//VALIDATION
let inputForm = document.querySelectorAll('.modal-form-input')
let inputBlock = document.getElementById('modal-form-input-block')
let errorMessage = document.createElement('span')

class CardsError {
	constructor (message) {
		this.message = message
		this.name = 'CardsError: '
	}

	error() {
		inputForm.forEach(el => {
			if (el.value === '') {
				el.style.borderColor = 'red'
			}
			let error = document.getElementById('modal-error')
			if(!error) {
				inputBlock.appendChild(errorMessage)
			}
			errorMessage.style.color = 'red'
			errorMessage.id = 'modal-error'
			errorMessage.innerText = this.name + this.message
			errorMessage.style.display = 'block'
		})
	}
}

createCard.onclick = () => {
	try {
		inputForm.forEach(el => {
			if (el.value === '' && el.style.display === 'block') {
				throw new CardsError('Invalid value')
			}
			el.style.borderColor = '#b8860b'
			errorMessage.style.display = 'none'
		})
		let dataObj = {}
		dataObj.doctor = form.querySelector('select').value
		let inputs = form.querySelectorAll('input')
		for (let i = 0; i < inputs.length; i++)
			dataObj[inputs[i].dataset.name] = inputs[i].value
		dataObj.comment = form.querySelector('textarea').value

		newCard(dataObj)
		modal.style.display = "none"

	} catch (e) {
		e.error()
	}
//
}

//FILTER
function filterForm() {

	let selectForm = document.getElementById('select-form').value
	let inputForm = document.querySelectorAll('.modal-form-input')

	this.cardiologist = ['name', 'age', 'purpose', 'pressure', 'weight', 'illness']
	this.dentist = ['name', 'purpose', 'lastVisit']
	this.therapist = ['name', 'age', 'purpose']

	inputForm.forEach((el) => {
		el.style.display = 'none'
		errorMessage.style.display = 'none'
		el.style.borderColor = '#b8860b'

		if (selectForm === 'Cardiologist' && this.cardiologist.includes(el.dataset.name)) {
			el.style.display = 'block'
		} else if (selectForm === 'Dentist' && this.dentist.includes(el.dataset.name)) {
			el.style.display = 'block'
		} else if (selectForm === 'Therapist' && this.therapist.includes(el.dataset.name)) {
			el.style.display = 'block'
		}
	})
}

filterForm()




