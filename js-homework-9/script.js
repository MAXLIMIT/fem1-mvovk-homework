const tabTitle = document.querySelector('.tabs');
const tabContent = document.querySelector('.tabs-content');

tabTitle.addEventListener('click',(event) => {
    [...tabTitle.children].map(elem => {
        elem.classList.remove('active')
    });

    event.target.classList.add('active');

    [...tabContent.children].map(elem => {
        if(elem.dataset.name === event.target.innerText){
            elem.style.display = 'block';
        } else {
            elem.style.display = 'none';
        }
    });
});