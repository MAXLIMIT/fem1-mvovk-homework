const styleButton = document.getElementById('style-btn');
const styleMode = document.createElement('link');
      styleMode.rel = 'stylesheet';

      if(localStorage.getItem('theme') === 'dark-mode'){
          styleMode.href = 'style/dark-mode.css';
          styleButton.classList.add('dark-active');
      } else {
          styleMode.href = 'style/light-mode.css'
      }

      document.head.appendChild(styleMode);

      styleButton.addEventListener('click', event => {
          styleButton.classList.toggle('dark-active');
          if(styleButton.classList.contains('dark-active')) {
              styleMode.href = 'style/dark-mode.css';
              localStorage.setItem('theme', 'dark-mode')
          } else {
              styleMode.href = 'style/light-mode.css';
              localStorage.setItem('theme', 'light-mode')
          }
      });
