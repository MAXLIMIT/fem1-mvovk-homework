const btn = document.querySelectorAll('.btn');

document.addEventListener('keypress', (event)=>{
    btn.forEach((el) => {
        if(el.innerText.toLowerCase() === event.key.toLowerCase()) {
            el.style.backgroundColor = 'blue'
        } else {
            el.style.backgroundColor = '#33333a'
        }
    })
});