$('.tabs-title').on('click', function (e) {
    $(this).addClass('active')
           .siblings()
           .removeClass('active');

    const dataValue = $(e.target).text();

    $('.tabs-content').find(`[data-name = ${dataValue}]`)
                      .addClass('active-content')
                      .siblings()
                      .removeClass('active-content');
});
