class Hamburger {
    constructor (size, stuffing) {
        try {
            if (!size || !stuffing) {
                throw new HamburgerException('No size/stuffing given')
            }
            if (size.type !== 'size' || stuffing.type !== 'stuffing') {
                throw new HamburgerException('Invalid size or stuffing')
            }
            this.size = size;
            this.stuffing = stuffing;
            this.toppings = [];
    } catch (e) {
            console.log(e.name, e.message)
        }
    }

    addTopping (topping) {
        try {
            if (this.size === undefined || this.stuffing === undefined) {
                throw new HamburgerException('No size/stuffing given')
            }
            if (this.size.type !== 'size' || this.stuffing.type !== 'stuffing') {
                throw new HamburgerException('Invalid size or stuffing')
            }
            if (this.toppings.includes(topping)) {
                throw new HamburgerException('Duplicate topping ' + topping.name)
            }
            this.toppings.push(topping)
        } catch (e) {
            console.log(e.name, e.message)
        }
    };

    removeTopping (topping) {
        try {
            if (this.toppings.includes(topping) === false) {
                throw new HamburgerException(topping.name + ' not found')
            }
            this.toppings.forEach((element, index) => {
                if (element === topping) {
                    this.toppings.splice(index, 1)
                }
            })
        } catch (e) {
            console.log(e.name, e.message)
        }
    };

    getToppings () {
        return this.toppings
    };

    getSize () {
        return this.size.name;
    };

    getStuffing () {
        return this.stuffing.name;
    };

    calculatePrice () {
        let sumToppingPrice = 0;
        this.toppings.forEach(el => {
            sumToppingPrice += el.price
        });

        return this.size.price + this.stuffing.price + sumToppingPrice
    };

    calculateCcal () {
        let sumToppingCcal = 0;
        this.toppings.forEach(el => {
            sumToppingCcal += el.ccal
        });

        return this.size.ccal + this.stuffing.ccal + sumToppingCcal;
    };
}


Hamburger.SIZE_SMALL = {
    price: 50,
    ccal: 20,
    name: 'Small',
    type: 'size'
};

Hamburger.SIZE_LARGE = {
    price: 100,
    ccal: 40,
    name: 'Large',
    type: 'size'
};

Hamburger.STUFFING_CHEESE = {
    price: 10,
    ccal: 20,
    name: 'Cheese',
    type: 'stuffing'
};

Hamburger.STUFFING_SALAD = {
    price: 20,
    ccal: 5,
    name: 'Salad',
    type: 'stuffing'
};

Hamburger.STUFFING_POTATO = {
    price: 15,
    ccal: 10,
    name: 'Potato',
    type: 'stuffing'
};

Hamburger.TOPPING_MAYO = {
    price: 20,
    ccal: 5,
    name: 'Mayo',
    type: 'topping'
};

Hamburger.TOPPING_SPICE = {
    price: 15,
    ccal: 0,
    name: 'Spice',
    type: 'topping'
};

class HamburgerException {
    constructor (message) {
        this.name = 'HamburgerException:';
        this.message = message;
    };
}

let smallHamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);

smallHamburger.addTopping(Hamburger.TOPPING_MAYO);smallHamburger.addTopping(Hamburger.TOPPING_MAYO);
smallHamburger.addTopping(Hamburger.TOPPING_SPICE);
smallHamburger.removeTopping(Hamburger.TOPPING_SPICE);

console.log("Price: " + smallHamburger.calculatePrice() + " грн. /", "ccal: " + smallHamburger.calculateCcal() + " кал. /" , "Size: " + smallHamburger.getSize(), "/ Stuffing: " + smallHamburger.getStuffing(), smallHamburger.getToppings());
