const documentList = (array) => {
    let list = document.createElement('ul');
    array.map((item) => {
        let listItem = document.createElement('li');
        listItem.innerText = item;
        list.appendChild(listItem)
    });
    return document.body.appendChild(list);
};

console.log(documentList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']));

